
	<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newvo
 */

 /*Template Name: Apropos*/ 
$image = get_field('dude_image');
$picture = $image['sizes']['large'];

get_header();
?>

		
<!-- <main id="primary" class="site-main"> -->
<main>
	<div class="wrapper">
    	<article class="produit-title">
    	    <h1 class="h1--darkgray">
				<?php the_field('titre'); ?> 
    	    </h1>
    	</article>
    </div>
  
    <section class="woodbg">
        <div class="wrapper">
            <div class="content--column">
                <div class="ordre">
                    <p class="texte--contact">
						<?php the_field('texte_desc'); ?> 
                    </p>
                </div>
              
              <div class="buttonflex">
                  <button class="gbutton--card">
                      <a href="produits-single.html">
                        Nous contacter
                      </a>
                  </button>
              </div>
            </div>
        </div>
    </section>

    <section class="wrapper">
      <div class="content">
          

         <img src="<?php echo $picture;?>" class="articleimg">
         
          
		
		  
        <div class="desc">
            <h3 class="texte--pboldcard">
			<?php the_field('h3_visionnaire'); ?>
			</h3>
            <br><br>
          <div class="article">
              <p class="texte--plightcard">
			  	<?php the_field('texte_dude'); ?> 
                  <br><br>
            	<?php the_field('texte2_dude'); ?> 
              </p>
          </div>

        </div>
      </div>
    </section>
</main>
	<?php
get_footer();


