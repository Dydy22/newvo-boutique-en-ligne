<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newvo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="wrapper">
		

		
		<?php
		if ( is_singular() ) :
		?>
			
				<?php 
					the_title( '<h1 class="h1--blogue">', '</h1>' );
					newvo_post_thumbnail();
				?>
		
		<?php
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		

		if ( 'post' === get_post_type() ) :
			?>

			
				<div class="nameflex--name">
					<p class="texte--name">
					<?php newvo_posted_by(); ?>
					</p>
					<p class="texte--profession pad--prof">
					<?php newvo_posted_on();?>
					</p>
				</div>
		
		<?php endif; ?>
		<!-- .entry-header -->

	

		<div class="text-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'newvo' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		
		?>
		</div>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
