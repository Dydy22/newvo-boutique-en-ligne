<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Newvo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body>

    <header class="main-header main-flex">
		<div class="wrapper">
			<nav class="main-nav wrapper">
				<a href="#">
					<img src="<?php bloginfo('template_directory');?>/img/logoNav.png" alt="Newvo logo" class="logo">
				</a>

				<div class="main-nav">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'nav',
								'menu_id'        => 'main-menu',
								'container' => 'ul',
								'menu_class' => 'main-nav',
							)
						);
						?> 
				</div>
			</nav>
		</div>
	</header><!-- #masthead -->
