<?php
/*
Template Name: page-contact
*/

get_header();
?>
<main class="wrapper">
      <section class="produit-title">
        <h1 class="h1--darkgray">
          Contact
        </h1>
      </section>

      <article class="content--row articlefix">
        <div class="articleflex">
          <p class="text--lp"> <?php the_field('adresse_postale'); ?> </p>
          <p class="text--lp pad"> <a href="tel:<?php the_field('telephone'); ?>"><?php the_field('telephone'); ?></a> </p>
          <p class="text--lp pad"> <a href="mailto:<?php the_field('adresse_courriel'); ?>"><?php the_field('adresse_courriel'); ?></a> </p>
        </div>

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2730.377924036885!2d-71.33334228414779!3d46.81655855020949!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cb8982adb7555f9%3A0x510da1498d9a2fbd!2sAmeublements%20Tanguay%20-%20Les%20Saules!5e0!3m2!1sfr!2sca!4v1600353132330!5m2!1sfr!2sca" width="574" height="340" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </article>

  </main>

<?php
get_footer();

