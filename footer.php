<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Newvo
 */

?>
	<footer id="colophon" class="site-footer main-footer">

		<div class="wrapper footer-flex">
			<div class="footer-logos">
				<img src="<?php bloginfo('template_directory')?>/img/logoFoot.png" alt="Newvo logo" class="footlogo">
			
				<div class="social-logos">
					<img src="<?php bloginfo('template_directory')?>/img/Facebook.png" alt="Facebook logo" class="social-items">
					<img src="<?php bloginfo('template_directory')?>/img/Twitter.png" alt="Twitter logo" class="social-items">
					<img src="<?php bloginfo('template_directory')?>/img/Instagram.png" alt="Instagram logo" class="social-items">
					<img src="<?php bloginfo('template_directory')?>/img/Pinterest.png" alt="Pinterest logo" class="social-items">
				</div>
			</div>
			<div class="footer-link">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'footer',
							'menu_id'        => 'footer',
							'menu_class' => 'main-footer',
						)
					);
					?> 
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
