<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newvo
 */

get_header();
?>
<main>
      <section class="woodbg">
        <div class="wrapper">
            <div class='titre'>
                  <h1 class="h1--cream">
                    <?php the_field('page_dacceuil_slogan'); ?>
                  </h1>
            </div>
        </div>
      </section>
      <section class="wrapper">
        <div class="content">
          <iframe src="https://www.youtube.com/embed/TiC8pig6PGE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="videoborder">
         </iframe>
        </div>
      </section>
      <section class="contact">
        <div class="wrapper">
          <div class="content--column">
            <div class="ordre">
              <p class="texte--contact">
                <?php the_field('page_daccueil_description'); ?> 
            </p>
          </div>
            <div class="buttonflex">
            
              <button class="gbutton--wbutton">
                <a href="<?php get_site_url( ); ?>/contact/">Nous contacter</a>                
              </button>
            </div>
          </div>
        </div>
      </section>
    </main>
<?php
get_footer();
