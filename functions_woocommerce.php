<?php
/**
 * Newvo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Newvo
 */

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

add_filter('loop_shop_rows', 'loop_rows', 999);
if (!function_exists('loop_rows')) {
	function loop_rows() {
		return 3; // 3 products per columns
	}
}

if ( ! function_exists( 'woocommerce_template_loop_product_title' ) ) {

	/**
	 * Show the product title in the product loop. By default this is an H2.
	 */
	function woocommerce_template_loop_product_title() {
		echo '<p class="texte--plightcard ">' . get_the_title() . '</p>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
	
}

if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {

	/**
	 * Get the product thumbnail, or the placeholder if not set.
	 *
	 * @param string $size (default: 'woocommerce_thumbnail').
	 * @param int    $deprecated1 Deprecated since WooCommerce 2.0 (default: 0).
	 * @param int    $deprecated2 Deprecated since WooCommerce 2.0 (default: 0).
	 * @return string
	 */
	function woocommerce_get_product_thumbnail( $size = 'woocommerce_thumbnail', $deprecated1 = 0, $deprecated2 = 0 ) {
		global $product;

		$image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );

		return $product ? "<div>" . $product->get_image( $image_size ) . "</div>" : '';
	}
}

function woocommerce_product_category( $args = array() ) {
	$woocommerce_category_id = get_queried_object_id();
	$args = array(
		'parent' => $woocommerce_category_id
	);
	$terms = get_terms( 'product_cat', $args );

	if ( $terms ) {
		echo '<section class="slide">';
		foreach ( $terms as $term ) {
			echo '<article class="card card--boutique-categorie woocommerce-product-category-page">';
			// woocommerce_subcategory_thumbnail( $term );
			
					// echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '">';
					echo '<a href="' .  esc_url( get_term_link( $term ) ) . '">';
						echo '<button class="gbutton--wbutton ' . $term->slug . '">';
							echo $term->name;
						echo '</button>';
					
					echo '</a>';
			echo '</article>';
		}
		echo '</section>';
	}
}
add_action( 'woocommerce_before_shop_loop', 'woocommerce_product_category', 100  );


function disable_woo_commerce_sidebar() {
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
}
add_action('init', 'disable_woo_commerce_sidebar');